<?php
/**
 * @file
 * ipage_seo.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function ipage_seo_defaultconfig_features() {
  return array(
    'ipage_seo' => array(
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function ipage_seo_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_transliterate';
  $strongarm->value = 1;
  $export['pathauto_transliterate'] = $strongarm;

  return $export;
}
